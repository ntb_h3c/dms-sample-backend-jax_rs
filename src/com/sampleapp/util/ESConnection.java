package com.sampleapp.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

public class ESConnection {
	private static TransportClient client;
	private static Settings settings;
	
	@SuppressWarnings("resource")
	public static TransportClient getClient(){
		settings = Settings.builder()
                .put("cluster.name","elasticsearch").build();
        
        try{
        	client = new PreBuiltTransportClient(settings)
			.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("127.0.0.1"),9300));
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return client;
	}

}

package com.sampleapp.services;

import java.util.Date;
import java.util.List;

import com.sampleapp.models.Movie;

public interface MovieService {
	void AddMovie(Movie m);
	long DeleteMovie(String imdbId);
	void UpdateMovie(String imdbId, Movie m);
	List<Movie> getAllMovies();
	List<Movie> searchMovieByTitle(String title);
	Movie searchMovieByImdb(String imdb);
	List<Movie> searchMovieByReleaseCountry(String country);
	List<Movie> searchMovieByReleaseDate(Date date);
}
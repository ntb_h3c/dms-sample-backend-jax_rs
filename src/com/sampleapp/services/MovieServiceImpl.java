package com.sampleapp.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.elasticsearch.action.bulk.byscroll.BulkByScrollResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryAction;
import org.elasticsearch.index.reindex.UpdateByQueryAction;
import org.elasticsearch.search.SearchHit;
import org.json.JSONObject;

import com.sampleapp.models.Movie;
import com.sampleapp.util.ESConnection;


/**
 * Session Bean implementation class MovieServiceImpl
 */
@Stateless
@LocalBean
public class MovieServiceImpl implements MovieService{

	private TransportClient client;
    /**
     * Default constructor. 
     */
    public MovieServiceImpl() {
        // TODO Auto-generated constructor stub
    	client = ESConnection.getClient();
    }

	@SuppressWarnings("deprecation")
	@Override
	public void AddMovie(Movie m) {
		// TODO Auto-generated method stub
//		String json = "{"+
//						"\"title\":\""+m.getTitle()+"\","+
//						"\"imdbId\":\""+m.getImdbId()+"\","+
//						"\"releaseDate\":\""+m.getReleaseDate()+"\","+
//						"\"releaseCountry\":\""+m.getReleaseCountry()+"\","+
//						"\"releaseYear\":\""+m.getReleaseYear()+"\","+
//						"\"releaseMonth\":\""+m.getReleaseMonth()+"\","+
//						"\"releaseDay\":\""+m.getReleaseDay()+"\""+
//					"}";
		String json = "{"+
				"\"title\":\""+m.getTitle()+"\","+
				"\"imdbId\":\""+m.getImdbId()+"\","+
				"\"releaseDate\":\""+m.getReleaseDate()+"\","+
				"\"releaseCountry\":\""+m.getReleaseCountry()+"\""+
			"}";
		
		IndexResponse response = client.prepareIndex("movies","movie")
				.setSource(json)
				.get();
	}

	@Override
	public long DeleteMovie(String imdbId) {
		// TODO Auto-generated method stub
		BulkByScrollResponse response = DeleteByQueryAction.INSTANCE.newRequestBuilder(client)
			.filter(QueryBuilders.matchQuery("imdbId", imdbId))
			.source("movies")
			.get();
		
        return response.getDeleted();
	}

	@Override
	public void UpdateMovie(String imdbId, Movie m) {
		// TODO Auto-generated method stub
		String json = "{"+
				"\"title\":\""+m.getTitle()+"\","+
				"\"imdbId\":\""+m.getImdbId()+"\","+
				"\"releaseDate\":\""+m.getReleaseDate()+"\","+
				"\"releaseCountry\":\""+m.getReleaseCountry()+"\""+
			"}";
		
		BulkByScrollResponse response = UpdateByQueryAction.INSTANCE.newRequestBuilder(client)
				.filter(QueryBuilders.matchQuery("imdbId", imdbId))
				.source("movies")
				.get();
	}

	@Override
	public List<Movie> getAllMovies() {
		SearchResponse res = client.prepareSearch("movies").setTypes("movie").get();
		SearchHit[] results = res.getHits().getHits();
		List<Movie> movies = new ArrayList<>();
		
		for(SearchHit item : results){
			System.out.println(item.sourceAsString());
			String title = item.getField("title").getValue().toString();
			String imdbId = item.getField("imdbId").getValue().toString();
			String releaseCountry = item.getField("releaseCountry").getValue().toString();
			String releaseDate = item.getField("releaseDate").getValue().toString();
			try {
				movies.add(new Movie(title,imdbId,releaseCountry,releaseDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return movies;
	}
	
//	public List<SearchHit> getAllMovieHits(){
//		SearchResponse res = client.prepareSearch("movies").setTypes("movie").get();
//		SearchHit[] results = res.getHits().getHits();
//		List<SearchHit> movies = new ArrayList<>();
//		
//		for(SearchHit item : results){
//			movies.add(item);
//		}
//		
//		return movies;
//	}
//	
//	public JSONObject getAllMovieHits_JSON(){
//		SearchResponse res = client.prepareSearch("movies").setTypes("movie").get();
//		SearchHit[] results = res.getHits().getHits();
//		JSONObject movies = new JSONObject();
//		
//		for(SearchHit item : results){
//			movies.put(item.getId(),item);
//		}
//		
//		return movies;
//	}

	@Override
	public List<Movie> searchMovieByTitle(String title) {
		SearchResponse res = client.prepareSearch("movies")
				.setTypes("movie")
				.setQuery(QueryBuilders.fuzzyQuery("title", title))
				.get();
		
		SearchHit[] results = res.getHits().getHits();
		List<Movie> movies = new ArrayList<>();
		
		for(SearchHit item : results){
			String title_m = item.getField("title").getValue().toString();
			String imdbId = item.getField("imdbId").getValue().toString();
			String releaseCountry = item.getField("releaseCountry").getValue().toString();
			String releaseDate = item.getField("releaseDate").getValue().toString();
			try {
				movies.add(new Movie(title_m,imdbId,releaseCountry,releaseDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return movies;
	}

	@Override
	public Movie searchMovieByImdb(String imdb) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Movie> searchMovieByReleaseCountry(String country) {
		SearchResponse res = client.prepareSearch("movies")
				.setTypes("movie")
				.setQuery(QueryBuilders.fuzzyQuery("releaseCountry", country))
				.get();
		
		SearchHit[] results = res.getHits().getHits();
		List<Movie> movies = new ArrayList<>();
		
		for(SearchHit item : results){
			String title_m = item.getField("title").getValue().toString();
			String imdbId = item.getField("imdbId").getValue().toString();
			String releaseCountry = item.getField("releaseCountry").getValue().toString();
			String releaseDate = item.getField("releaseDate").getValue().toString();
			try {
				movies.add(new Movie(title_m,imdbId,releaseCountry,releaseDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return movies;
	}

	@Override
	public List<Movie> searchMovieByReleaseDate(Date date) {
		SearchResponse res = client.prepareSearch("movies")
				.setTypes("movie")
				.setQuery(QueryBuilders.fuzzyQuery("releaseDate", date.toString()))
				.get();
		
		SearchHit[] results = res.getHits().getHits();
		List<Movie> movies = new ArrayList<>();
		
		for(SearchHit item : results){
			String title_m = item.getField("title").getValue().toString();
			String imdbId = item.getField("imdbId").getValue().toString();
			String releaseCountry = item.getField("releaseCountry").getValue().toString();
			String releaseDate = item.getField("releaseDate").getValue().toString();
			try {
				movies.add(new Movie(title_m,imdbId,releaseCountry,releaseDate));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return movies;
	}

}
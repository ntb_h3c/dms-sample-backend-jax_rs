package com.sampleapp.models;

import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;

public class Movie {
	private String title;
	private String imdbId;
	private String releaseCountry;
	private String releaseDate;
	private int releaseYear;
	private int releaseMonth;
	private int releaseDay;
	
	public Movie(){}
		
	public Movie(String title, String imdbId, String releaseCountry, String releaseDate) throws ParseException {
		super();
		this.title = title;
		this.imdbId = imdbId;
		this.releaseCountry = releaseCountry;
		this.releaseDate = releaseDate;
		
//	    SimpleDateFormat formatter =new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");  
//
//		this.releaseDate = formatter.parse(releaseDate);
//		
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(this.releaseDate);
//		
//		this.releaseYear = cal.get(Calendar.YEAR);
//		this.releaseMonth = cal.get(Calendar.MONTH);
//		this.releaseDay = cal.get(Calendar.DATE);
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getImdbId() {
		return imdbId;
	}
	
	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}
	
	public String getReleaseCountry() {
		return releaseCountry;
	}
	
	public void setReleaseCountry(String releaseCountry) {
		this.releaseCountry = releaseCountry;
	}
	
//	public Date getReleaseDate() {
//		return releaseDate;
//	}
//	
//	public void setReleaseDate(Date releaseDate) {
//		this.releaseDate = releaseDate;
//	}
	
	public String getReleaseDate() {
		return releaseDate;
	}
	
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	
	public int getReleaseYear() {
		return releaseYear;
	}
	
	public void setReleaseYear(int releaseYear) {
		this.releaseYear = releaseYear;
	}
	
	public int getReleaseMonth() {
		return releaseMonth;
	}
	
	public void setReleaseMonth(int releaseMonth) {
		this.releaseMonth = releaseMonth;
	}
	
	public int getReleaseDay() {
		return releaseDay;
	}
	
	public void setReleaseDay(int releaseDay) {
		this.releaseDay = releaseDay;
	}
	
	
}

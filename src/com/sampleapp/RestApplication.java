package com.sampleapp;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.sampleapp.controllers.MovieController;

@ApplicationPath("movieService")
public class RestApplication extends Application{
	@Override
    public Set<Class<?>> getClasses() {
        HashSet<Class<?>> set = new HashSet<Class<?>>();
        set.add(MovieController.class);
        return set;
    }
}

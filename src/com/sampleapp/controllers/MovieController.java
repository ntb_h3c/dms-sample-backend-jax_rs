package com.sampleapp.controllers;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.sampleapp.services.MovieServiceImpl;

@Path("/movies")
public class MovieController {
	
	@EJB
	MovieServiceImpl movieService;

//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response hello(){
//		
//		return Response.status(Response.Status.OK).entity(movieService.getAllMovieHits_JSON()).build();
//		
////		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unexpected Error. Please contact support").build();
//	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllMovies(){
		ObjectMapper objectMaper = new ObjectMapper();
		try {
			return Response.status(Response.Status.OK).entity(objectMaper.writeValueAsString((movieService.getAllMovies()))).build();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unexpected Error. Please contact support").build();
	}
	
	@GET
	@Path("/title/{title}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMoviesByTitle(@PathParam("title") String title){
		ObjectMapper objectMapper = new ObjectMapper();
		try{
			return Response.status(Response.Status.OK)
					.entity(objectMapper.writeValueAsString(movieService.searchMovieByTitle(title))).build();
		}catch(Exception e){
			e.printStackTrace();
		}
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unexpected Error. Please contact support").build();
	}
	
	@GET
	@Path("/imdb /{imdb}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMoviesByImdbId(@PathParam("imdb") String imdb){
		ObjectMapper objectMapper = new ObjectMapper();
		try{
			return Response.status(Response.Status.OK)
					.entity(objectMapper.writeValueAsString(movieService.searchMovieByImdb(imdb))).build();
		}catch(Exception e){
			e.printStackTrace();
		}
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unexpected Error. Please contact support").build();
	}
	
	@GET
	@Path("/country /{country}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMoviesByCountry(@PathParam("country") String country){
		ObjectMapper objectMapper = new ObjectMapper();
		try{
			return Response.status(Response.Status.OK)
					.entity(objectMapper.writeValueAsString(movieService.searchMovieByReleaseCountry(country))).build();
		}catch(Exception e){
			e.printStackTrace();
		}
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unexpected Error. Please contact support").build();
	}
	
	@GET
	@Path("/date /{date}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMoviesByDate(@PathParam("date") String date){
		ObjectMapper objectMapper = new ObjectMapper();
		try{
//			return Response.status(Response.Status.OK)
//					.entity(objectMapper.writeValueAsString(movieService.searchMovieByReleaseDate(date))).build();
		}catch(Exception e){
			e.printStackTrace();
		}
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unexpected Error. Please contact support").build();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response addNewMovie(){
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unexpected Error. Please contact support").build();
	}
	
	@PUT
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateMovie(@PathParam("id") String imdb){
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unexpected Error. Please contact support").build();
	}
	
	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteMovie(@PathParam("id") String imdb){
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Unexpected Error. Please contact support").build();
	}
	
	
	
}
